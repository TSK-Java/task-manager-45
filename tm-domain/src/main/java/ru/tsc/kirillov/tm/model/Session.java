package ru.tsc.kirillov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractModel {

    @Nullable
    private User user;

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}
