package ru.tsc.kirillov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
