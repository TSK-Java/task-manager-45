package ru.tsc.kirillov.tm.exception.user;

import lombok.NoArgsConstructor;
import ru.tsc.kirillov.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractUserException extends AbstractException {

    public AbstractUserException(String message) {
        super(message);
    }

    public AbstractUserException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(Throwable cause) {
        super(cause);
    }

    public AbstractUserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
