package ru.tsc.kirillov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kirillov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.dto.model.UserDTO;
import ru.tsc.kirillov.tm.dto.request.UserLoginRequest;
import ru.tsc.kirillov.tm.dto.request.UserProfileRequest;
import ru.tsc.kirillov.tm.dto.response.UserLoginResponse;
import ru.tsc.kirillov.tm.dto.response.UserProfileResponse;
import ru.tsc.kirillov.tm.marker.ISoapCategory;
import ru.tsc.kirillov.tm.service.PropertyService;

import java.util.UUID;

@Category(ISoapCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final String userLogin = "test";

    @NotNull
    private final String userPassword = "test";

    @Test
    public void login() {
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(
                    new UserLoginRequest(UUID.randomUUID().toString(), UUID.randomUUID().toString())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(
                        new UserLoginRequest(userLogin, UUID.randomUUID().toString())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(
                        new UserLoginRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(
                        new UserLoginRequest(userLogin, null)
                )
        );
        @NotNull final UserLoginResponse response =
                authEndpoint.login(new UserLoginRequest(userLogin, userPassword));
        Assert.assertNotNull(response);
        Assert.assertTrue(response.getSuccess());
        Assert.assertNotNull(response.getToken());
    }

    @Test
    public void profile() {
        @NotNull final UserLoginResponse response =
                authEndpoint.login(new UserLoginRequest(userLogin, userPassword));
        Assert.assertNotNull(response);
        @Nullable String token = response.getToken();
        Assert.assertNotNull(token);
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest()));
        Assert.assertThrows(
                Exception.class, () -> authEndpoint.profile(new UserProfileRequest(""))
        );
        Assert.assertThrows(
                Exception.class, () -> authEndpoint.profile(new UserProfileRequest(null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.profile(new UserProfileRequest(UUID.randomUUID().toString()))
        );
        UserProfileResponse responseProfile = authEndpoint.profile(new UserProfileRequest(token));
        Assert.assertNotNull(responseProfile);
        @Nullable UserDTO user = responseProfile.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(userLogin, user.getLogin());
    }

}
