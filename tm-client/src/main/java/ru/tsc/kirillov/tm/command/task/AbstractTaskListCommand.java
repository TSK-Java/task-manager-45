package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.TaskDTO;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;

import java.util.List;

public abstract class AbstractTaskListCommand extends AbstractTaskCommand {

    protected void printTask(@Nullable final List<TaskDTO> tasks) {
        if (tasks == null) throw new TaskNotFoundException();
        int idx = 0;
        for(final TaskDTO task: tasks) {
            if (task == null) continue;
            System.out.println(++idx + ". " + task);
        }
    }

}
