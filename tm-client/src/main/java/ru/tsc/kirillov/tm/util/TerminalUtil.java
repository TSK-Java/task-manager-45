package ru.tsc.kirillov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.exception.system.value.ValueNotValidInteger;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @Nullable String valueStr = null;
        try {
            valueStr = nextLine();
            return Integer.parseInt(valueStr);
        }
        catch (@NotNull final NumberFormatException e) {
            @NotNull ValueNotValidInteger childException = new ValueNotValidInteger(valueStr);
            childException.initCause(e);
            throw childException;
        }
    }

    @NotNull
    static Date nextDate() {
        return DateUtil.toDate(nextLine());
    }

}
