package ru.tsc.kirillov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.kirillov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

}
