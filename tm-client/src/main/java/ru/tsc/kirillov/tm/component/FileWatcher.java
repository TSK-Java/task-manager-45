package ru.tsc.kirillov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.component.IFileListener;
import ru.tsc.kirillov.tm.enumerated.FileEventKind;
import ru.tsc.kirillov.tm.event.FileEvent;

import java.io.File;
import java.nio.file.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.nio.file.StandardWatchEventKinds.*;

public final class FileWatcher {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<IFileListener> listeners = new CopyOnWriteArrayList<>();

    @NotNull
    private final File folder;

    public FileWatcher(@NotNull final File folder) {
        this.folder = folder;
    }

    @SneakyThrows
    private boolean pollEvents(@NotNull final WatchService watchService) {
        @NotNull final WatchKey key = watchService.take();
        @NotNull final Path path = (Path) key.watchable();
        for (@NotNull final WatchEvent<?> event : key.pollEvents()) {
            notifyListeners(event.kind(), path.resolve((Path) event.context()).toFile());
        }
        return key.reset();
    }

    private void notifyListeners(@NotNull final WatchEvent.Kind<?> kind, @NotNull final File file) {
        @NotNull FileEventKind fileEventKind;
        if (kind == ENTRY_CREATE) {
            fileEventKind = FileEventKind.CREATE;
        } else if (kind == ENTRY_MODIFY) {
            fileEventKind = FileEventKind.MODIFY;
        } else if (kind == ENTRY_DELETE) {
            fileEventKind = FileEventKind.DELETE;
        } else if (kind == OVERFLOW) {
            fileEventKind = FileEventKind.OVERFLOW;
        } else return;
        @NotNull final FileEvent event = new FileEvent(file, fileEventKind);
        for (@NotNull final IFileListener listener : listeners) {
            listener.onEvent(event);
        }
    }

    @SneakyThrows
    private void run() {
        try (@NotNull final WatchService watchService =
                     FileSystems.getDefault().newWatchService()) {
            @NotNull final Path path = Paths.get(folder.getAbsolutePath());
            path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE, OVERFLOW);
            while (pollEvents(watchService));
        }
    }

    public void startWatch() {
        es.execute(this::run);
    }

    public void stopWatch() {
        es.shutdown();
    }

    public void addListener(@NotNull final IFileListener listener) {
        listeners.add(listener);
    }

    public void removeListener(@NotNull final IFileListener listener) {
        listeners.remove(listener);
    }

}
