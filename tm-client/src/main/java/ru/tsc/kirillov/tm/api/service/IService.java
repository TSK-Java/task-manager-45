package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IRepository;
import ru.tsc.kirillov.tm.dto.model.AbstractModelDTO;
import ru.tsc.kirillov.tm.enumerated.Sort;

import java.util.List;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable Sort sort);

}
