package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;
import ru.tsc.kirillov.tm.repository.dto.ProjectRepositoryDTO;

import javax.persistence.EntityManager;

public final class ProjectServiceDTO extends AbstractUserOwnedServiceDTO<ProjectDTO, IProjectRepositoryDTO> implements IProjectServiceDTO {

    public ProjectServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepositoryDTO(entityManager);
    }

    @NotNull
    @Override
    public String[] findAllId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            return repository.findAllId(userId);
        } finally {
            entityManager.close();
        }
    }

}
