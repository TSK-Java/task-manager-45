package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUserName();

    @NotNull
    String getDatabaseUserPassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseFormatSql();

    @NotNull
    String getDatabaseUseL2Cache();

    @NotNull
    String getDatabaseProviderConfigFileResourcePath();

    @NotNull
    String getDatabaseRegionFactoryClass();

    @NotNull
    String getDatabaseUserQueryCache();

    @NotNull
    String getDatabaseUseMinimalPuts();

    @NotNull
    String getDatabaseUseLiteMember();

    @NotNull
    String getDatabaseRegionPrefix();

}
