package ru.tsc.kirillov.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.Task;

public interface ITaskService extends IUserOwnedService<Task> {

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

}
