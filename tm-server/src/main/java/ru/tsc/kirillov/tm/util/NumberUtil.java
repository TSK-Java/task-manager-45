package ru.tsc.kirillov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class NumberUtil {

    @NotNull
    public static String formatBytes(final long bytes) {
        final long kilobyte = 1024;
        final long megabyte = kilobyte * 1024;
        final long gigabyte = megabyte * 1024;
        final long terabyte = gigabyte * 1024;

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";
        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return (bytes / kilobyte) + " KiB";
        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return (bytes / megabyte) + " MiB";
        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return (bytes / gigabyte) + " GiB";
        } else if (bytes >= terabyte) {
            return (bytes / terabyte) + " TiB";
        } else {
            return bytes + " Bytes";
        }
    }

    @Nullable
    public static Integer fixIndex(@Nullable Integer index) {
        if (index == null) return null;
        return --index;
    }

}
