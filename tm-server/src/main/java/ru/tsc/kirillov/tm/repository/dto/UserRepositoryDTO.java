package ru.tsc.kirillov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.kirillov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Optional;

public class UserRepositoryDTO extends AbstractRepositoryDTO<UserDTO> implements IUserRepositoryDTO {

    public UserRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(UserDTO.class, entityManager);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format("FROM %s m WHERE m.login = :login", getModelName());
        @NotNull final TypedQuery<UserDTO> query = entityManager.createQuery(jpql, clazz)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login);
        return getResult(query);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String jpql = String.format("FROM %s m WHERE m.email = :email", getModelName());
        @NotNull final TypedQuery<UserDTO> query = entityManager.createQuery(jpql, clazz)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email);
        return getResult(query);
    }

    @Nullable
    @Override
    public UserDTO removeByLogin(@NotNull final String login) {
        @NotNull final Optional<UserDTO> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

}
