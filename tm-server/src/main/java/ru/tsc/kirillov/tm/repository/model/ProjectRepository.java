package ru.tsc.kirillov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.repository.model.IProjectRepository;
import ru.tsc.kirillov.tm.model.Project;

import javax.persistence.EntityManager;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(Project.class, entityManager);
    }

}
