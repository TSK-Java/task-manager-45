package ru.tsc.kirillov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.kirillov.tm.dto.model.AbstractWbsModelDTO;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;

import java.util.Date;
import java.util.List;

public interface IUserOwnedServiceDTO<M extends AbstractWbsModelDTO> extends IUserOwnedRepositoryDTO<M>, IServiceDTO<M> {

    @Nullable
    M create(@Nullable String userId, @Nullable String name);

    @Nullable
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    M updateByIndex(
            @Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @Nullable
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
